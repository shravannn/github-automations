import helium as hl
import validators
import string
import random
import requests
from bs4 import BeautifulSoup

def validate_link(link):
    """
    Validates link.
    """
    if not validators.url(link):
        raise Exception("Invalid link!")

    if len(link.split("/")) != 5:
        raise Exception("Invalid link!")

    if link.split("/")[2] != "github.com":
        raise Exception("Invalid link!")

    r = requests.get(link).content
    soup = BeautifulSoup(r, "html5lib")
    title = soup.find("title")
    if title == "Page not found":
        raise Exception("This repo doesnt exists!")

def issue_title_and_description():
    """
    Returns a random title and description for the issue to be opened.
    """
    title = ""
    description = ""
    letters = string.ascii_letters
    for _ in range(20):
        title += letters[random.randint(0, len(letters)-1)]
    for _ in range(100):
        description += letters[random.randint(0, len(letters)-1)]
    
    return title, description

def login(username:str, password:str):
    """
    Logs into GitHub.\n
    @params
    username --> type str --> Your GitHub username.
    password --> type str --> Your GitHub password.
    """
    hl.write(username, into="Username or email address")
    hl.write(password, into="Password")
    hl.press(hl.ENTER)

def open_issue(username:str, password:str, repo_link:str, n:int):
    """
    Opens issues in GitHub repo passed as argument.

    @params
    repo_link --> type `str` --> Link to the GitHub repo you want to open issue.
    n --> type `int` --> Number of issues to be opened in the repo.
    """
    validate_link(repo_link)

    hl.start_chrome("https://github.com/login")
    login(username, password)

    for _ in range(n):
        hl.go_to(repo_link+"/issues")
        
        hl.click("New issue")
        
        title, description = issue_title_and_description()
        repo = repo_link.split("/")[-1]
        hl.write(title, into="Title")
        hl.write(description, into="Leave a comment")

        hl.click("Submit new issue")
        print(f"Opened issue titled `{title}` with description `{description}` in repo `{repo}`. \n")

    hl.kill_browser()
    print("Done!")


if __name__ == "__main__":
    open_issue("shravanasati", "Dx60@m!1", "https://github.com/VedantAsati/My_Projects", 5)
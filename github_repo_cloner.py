import subprocess
from os import chdir
directory = r"C:\Users\Lenovo\Documents\GitHub Open Source"
chdir(directory)

def https_cloning(repo_url):
    url = repo_url + ".git"
    subprocess.check_output(["git", "clone", url])
    print("Repo cloned successfully!")

def ssh_cloning(repo_url:str):
    # * example url git@github.com:sarzz2/File_Management_Server.git
    user = repo_url.split("/")[3]
    repo = repo_url.split("/")[4]
    ssh = f"git@github.com:{user}/{repo}.git"
    subprocess.check_output(["git", "clone", ssh])
    print("Repo cloned successfully!")

repo_url = input("Enter the repo url: ")
if len(repo_url.split("/")) != 5:
    raise Exception("iNVALID uRL")

mode = int(input("Which method to use while cloning?\n 1. SSH \n 2. HTTPS \n"))

if mode == 1:
    ssh_cloning(repo_url)

else:
    https_cloning(repo_url)

print("Press anything to exit...")
input()